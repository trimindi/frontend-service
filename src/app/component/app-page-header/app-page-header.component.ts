import { Component, HostBinding } from '@angular/core';

@Component({
    selector: 'app-page-header',
    templateUrl: './app-page-header.component.html',
    styleUrls: ['./app-page-header.component.css']
})
export class AppPageHeaderComponent {
    @HostBinding('class.ui')
    @HostBinding('class.masthead')
    @HostBinding('class.vertical')
    @HostBinding('class.segment')
    public classes:boolean = true;
}

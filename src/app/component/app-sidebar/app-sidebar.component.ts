import { Component } from '@angular/core';
import { navigation } from './../../_nav';

@Component({
  selector: 'app-sidebar',
  templateUrl: './app-sidebar.component.html',
  styleUrls: [ './app-sidebar.component.css']
})
export class AppSidebarComponent {
    public navigation = navigation;

    public isDivider(item) {
        return item.divider ? true : false;
    }

    public isTitle(item) {
        return item.title ? true : false;
    }

    constructor() { }
}
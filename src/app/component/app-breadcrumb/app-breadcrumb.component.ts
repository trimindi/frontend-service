
import { Component } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'app-breadcrumbs',
  template: `
  <ng-template ngFor let-breadcrumb [ngForOf]="breadcrumbs" let-last = last>
    <div class="ui breadcrumb"
        >
      <a *ngIf="!last" class="section" [routerLink]="breadcrumb.url">{{breadcrumb.label.title}}</a>
      <div *ngIf="!last" class="divider"> / </div>
      <div *ngIf="last" [routerLink]="breadcrumb.url" class="active section">{{breadcrumb.label.title}}</div>
    </div>
  </ng-template>`
})
export class AppBreadcrumpComponent {
    breadcrumbs: Array<Object>;
    constructor(
      private router: Router,
      private route: ActivatedRoute
    ) {
      this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event) => {
        this.breadcrumbs = [];
        let currentRoute = this.route.root,
        url = '';
        do {
          const childrenRoutes = currentRoute.children;
          currentRoute = null;
          // tslint:disable-next-line:no-shadowed-variable
          childrenRoutes.forEach(route => {
            if (route.outlet === 'primary') {
              const routeSnapshot = route.snapshot;
              console.log(routeSnapshot);
              url += '/' + routeSnapshot.url.map(segment => segment.path).join('/');
              this.pushToBradCrumpt(route.snapshot.data , url);
              currentRoute = route;
            }
          });
        } while (currentRoute);
      });
    }
    removeSlash(str){
      var lastChar = str.slice(-1);
      if (lastChar == '/') {
          return str = str.slice(0, -1);
      }
      return str;
    }
    pushToBradCrumpt(data, url){
      let str = this.removeSlash(url);
      let obj = {
        label: data,
        url:  str
      };
      if(!this.objectPropInArray(this.breadcrumbs,'url',str)){
        this.breadcrumbs.push(obj);
      }
    }
    objectPropInArray(list, prop, val) {
      if (list.length > 0 ) {
        for (let i in list) {
          if (list[i][prop] === val) {
            return true;
          }
        }
      }
      return false;
    }
  }
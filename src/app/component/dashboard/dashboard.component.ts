import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-dashboard-component',
    templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {
    items = [
        {
            title: 'Modul Setting',
            path: '/account',
            image: 'assets/images/avatar/matthew.png',
            description: 'Modul setting'
        },
        {
            title: 'Modul Anggota',
            path: '/anggota',
            image: 'assets/images/avatar/matthew.png',
            description: 'Modul Anggota'
        },
        {
            title: 'Modul Simpanan',
            path: '/anggota',
            image: 'assets/images/avatar/matthew.png',
            description: 'Modul Anggota'
        },
        {
            title: 'Modul Deposito',
            path: '/anggota',
            image: 'assets/images/avatar/matthew.png',
            description: 'Modul Anggota'
        },
        {
            title: 'Modul Hutang',
            path: '/anggota',
            image: 'assets/images/avatar/matthew.png',
            description: 'Modul Anggota'
        },
        {
            title: 'Modul Piutang',
            path: '/anggota',
            image: 'assets/images/avatar/matthew.png',
            description: 'Modul Anggota'
        },
        {
            title: 'Modul laporan',
            path: '/anggota',
            image: 'assets/images/avatar/matthew.png',
            description: 'Modul Anggota'
        }
    ];
    constructor() { }
    ngOnInit() { }
}

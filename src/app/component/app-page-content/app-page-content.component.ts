import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'app-page-content',
  templateUrl: './app-page-content.component.html',
  styleUrls: ['./app-page-content.component.css']
})
export class AppPageContentComponent { 
    @HostBinding('class.ui')
    @HostBinding('class.main')
    @HostBinding('class.container')
    public classes:boolean = true;
}

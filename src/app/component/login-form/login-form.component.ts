import { Store } from '@ngrx/store';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as fromRoot from './../../store';

@Component({
  selector: 'app-login-form',
  templateUrl: 'login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  auth: FormGroup;
  constructor(private fb: FormBuilder, private store: Store<fromRoot.State>
  ) { }
  ngOnInit() {
    this.auth = this.fb.group({
      username: ['', [Validators.required, Validators.minLength(4)]],
      password: ['', [Validators.required, Validators.minLength(4)]]
    });
  }
  onSubmit({ value, valid }: { value: any, valid: boolean }) {
    if (valid) {
      this.store.dispatch(new fromRoot.PerformLogin(value));
    }
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SuiCollapseModule, SuiPopupModule } from 'ng2-semantic-ui';

import { 
    AppFooterComponent,
    AppHeaderComponent,
    AppSidebarComponent,
    AppPageContentComponent,
    AppPageHeaderComponent,
    AppBreadcrumpComponent
} from './';
@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        SuiCollapseModule,
        SuiPopupModule
    ],
    declarations: [
        AppFooterComponent,
        AppSidebarComponent,
        AppHeaderComponent,
        AppPageContentComponent,
        AppPageHeaderComponent,
        AppBreadcrumpComponent
    ],
    exports: [
        AppFooterComponent,
        AppSidebarComponent,
        AppHeaderComponent,
        AppPageContentComponent,
        AppPageHeaderComponent,
        AppBreadcrumpComponent
    ]
})
export class ComponentModule {}

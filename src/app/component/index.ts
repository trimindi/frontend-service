import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginFormComponent } from './login-form/login-form.component';
export * from './app-footer/app-footer.component';
export * from './app-header/app-header.component';
export * from './app-sidebar/app-sidebar.component';
export * from './app-page-content/app-page-content.component';
export * from './app-page-header/app-page-header.component';
export * from './app-breadcrumb/app-breadcrumb.component';

export const components: any[] = [
    LoginFormComponent,
    DashboardComponent
];

import { AuthService } from './auth.service';
import { MasterService } from './master.service';
export const services: any[] = [
    AuthService,
    MasterService
];

export * from './auth.service';
export * from './master.service';


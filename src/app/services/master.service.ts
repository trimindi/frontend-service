import { IResponse, Pendidikan, Jabatan , Agama , Golongan, Identitas , Pekerjaan , HubunganKeluarga  } from './../models';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { catchError, map } from 'rxjs/operators';
import 'rxjs/add/observable/throw';

@Injectable()
export class MasterService {
  constructor(private http: HttpClient) {}

  getAllPendidikan(): Observable<Pendidikan[]> {
    return this.http
      .get<IResponse<Pendidikan[]>>(`/api/v1/master/pendidikan`)
      .pipe(
        map(r => r.data),
        catchError((error: any) => Observable.throw(error.json()))
      );
  }

  getAllJabatan(): Observable<Jabatan[]> {
    return this.http
      .get<IResponse<Jabatan[]>>(`/api/v1/master/jabatan`)
      .pipe(
        map(r => r.data),
        catchError((error: any) => Observable.throw(error.json()))
      );
  }
  getAllGolongan(): Observable<Golongan[]> {
    return this.http
      .get<IResponse<Golongan[]>>(`/api/v1/master/golongan`)
      .pipe(
        map(r => r.data),
        catchError((error: any) => Observable.throw(error.json()))
      );
  }

  getAllAgama(): Observable<Agama[]> {
    return this.http
      .get<IResponse<Agama[]>>(`/api/v1/master/golongan`)
      .pipe(
        map(r => r.data),
        catchError((error: any) => Observable.throw(error.json()))
      );
  }
  getAllIdentitas(): Observable<Identitas[]> {
    return this.http
      .get<IResponse<Identitas[]>>(`/api/v1/master/identitas`)
      .pipe(
        map(r => r.data),
        catchError((error: any) => Observable.throw(error.json()))
      );
  }
  getAllPekerjaan(): Observable<Pekerjaan[]> {
    return this.http
      .get<IResponse<Pekerjaan[]>>(`/api/v1/master/pekerjaan`)
      .pipe(
        map(r => r.data),
        catchError((error: any) => Observable.throw(error.json()))
      );
  }
  getAllHubunganKeluarga(): Observable<HubunganKeluarga[]> {
    return this.http
      .get<IResponse<HubunganKeluarga[]>>(`/api/v1/master/hub_keluarga`)
      .pipe(
        map(r => r.data),
        catchError((error: any) => Observable.throw(error.json()))
      );
  }
}


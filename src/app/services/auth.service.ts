import { IResponse } from './../models';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { catchError, map } from 'rxjs/operators';
import 'rxjs/add/observable/throw';

@Injectable()
export class AuthService {
  constructor(private http: HttpClient) {}

  performLogin(payload: any): Observable<Account> {
    const body = {
        username: payload.username,
        password: payload.password
    };
    return this.http
      .post(`/api/auth/login`, body)
      .pipe(
        catchError((error: any) => Observable.throw(error.json()))
      );
  }
}


import { MasterContainerComponent } from './master/master-container.component';
import { ApprovalContainerComponent } from './approval/approval-container.component';
import { DashboardComponent } from './dashboard/dashboard-container.component';
export const containers: any[] = [
    ApprovalContainerComponent,
    DashboardComponent,
    MasterContainerComponent
];

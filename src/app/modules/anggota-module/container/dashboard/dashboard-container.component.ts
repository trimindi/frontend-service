import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-dashboard-component',
    templateUrl: './dashboard-container.component.html'
})
export class DashboardComponent implements OnInit {
    items = [
        {
            title: 'Master Anggota',
            path: '/anggota/master',
            image: 'assets/images/avatar/matthew.png',
            description: 'Modul master anggota'
        },
        {
            title: 'Approval Anggota',
            path: '/account/approval',
            image: 'assets/images/avatar/matthew.png',
            description: 'Modul approval Anggota'
        }
    ];
    constructor() { }
    ngOnInit() { }
}

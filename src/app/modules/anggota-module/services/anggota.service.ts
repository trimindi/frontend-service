import { IResponse } from './../../../models/response.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { catchError, map } from 'rxjs/operators';
import 'rxjs/add/observable/throw';

import { Anggota } from '../models';

@Injectable()
export class AnggotaService {
  constructor(private http: HttpClient) {}

  getAnggota(): Observable<Anggota[]> {
    return this.http
      .get<IResponse<Anggota[]>>(`/api/v1/anggota`)
      .pipe(
        map(r => r.data),
        catchError((error: any) => Observable.throw(error.json()))
      );
  }

  createAnggota(payload: Anggota): Observable<Anggota> {
    return this.http
      .post<IResponse<Anggota>>(`/api/v1/anggota`, payload)
      .pipe(
        map(r => r.data),
        catchError((error: any) => Observable.throw(error.json()))
      );
  }

  updateAnggota(payload: Anggota): Observable<Anggota> {
    return this.http
      .put<IResponse<Anggota>>(`/api/v1/anggota/${payload.id}`, payload)
      .pipe(
        map(r => r.data),
        catchError((error: any) => Observable.throw(error.json()))
      );
  }

  removeAnggota(payload: Anggota): Observable<Anggota> {
    return this.http
      .delete<any>(`/api/v1/anggota/${payload.id}`)
      .pipe(catchError((error: any) => Observable.throw(error.json())));
  }
}


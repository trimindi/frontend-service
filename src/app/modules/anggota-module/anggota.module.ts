import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { NgModule } from '@angular/core';
import { ComponentModule } from '../../component/component.module';
import { AnggotaRoutingModule } from './anggota-routing.module';
import { effects, reducers } from './store';


import * as fromService from './services';
import * as fromUtils from './../../utils';
import * as fromComponent from './component';
import * as fromContainer from './container';
import * as fromGuard from './guards';
@NgModule({
  imports: [
    DataTablesModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    AnggotaRoutingModule,
    ComponentModule,
    StoreModule.forFeature('anggota', reducers),
    EffectsModule.forFeature(effects),
  ],
  declarations: [
    ...fromComponent.components,
    ...fromContainer.containers
  ],
  providers: [...fromService.services, ...fromUtils.inreceptor, ...fromGuard.guards],
  exports: [
    ...fromComponent.components,
    ...fromContainer.containers
  ]
})
export class AnggotaModule { }

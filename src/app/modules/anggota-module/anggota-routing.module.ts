import { ApprovalContainerComponent } from './container/approval/approval-container.component';
import { MasterContainerComponent } from './container/master/master-container.component';
import { DashboardComponent } from './container/dashboard';

import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    data: {
      title: 'Dashboard Anggota'
    }
  },
  {
    path: 'master',
    component: MasterContainerComponent,
    data: {
      title: 'Master Anggota'
    }
  },
  {
    path: 'approval',
    component: ApprovalContainerComponent,
    data: {
      title: 'Master Anggota'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnggotaRoutingModule {}

import { GO } from './../../../../store/actions/router.action';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import { map, switchMap, catchError } from 'rxjs/operators';

import * as fromRoot from './../../../../store';
import { AnggotaActionType } from './../actions';
import * as anggotaAction from './../actions/anggota.actions';
import * as fromServices from '../../services';

@Injectable()
export class AnggotaEffect {
  constructor(
    private actions$: Actions,
    private anggotaService: fromServices.AnggotaService
  ) {}

  @Effect()
  loadAnggota$ = this.actions$.ofType(AnggotaActionType.Load).pipe(
    switchMap(() => {
      return this.anggotaService
        .getAnggota()
        .pipe(
          map(account => new anggotaAction.LoadSuccess(account)),
          catchError(error => of(new anggotaAction.LoadFailed(error)))
        );
    })
  );

  @Effect()
  createAnggota$ = this.actions$.ofType(AnggotaActionType.Create).pipe(
    map((action: anggotaAction.Create) => action.payload),
    switchMap(anggota => {
      return this.anggotaService
        .createAnggota(anggota)
        .pipe(
          map(acc => new anggotaAction.CreateSuccess(acc)),
          catchError(error => of(new anggotaAction.CreateFailed(error)))
        );
    })
  );
}

import { AnggotaEffect } from './effects/anggota.effect';

export * from './actions';
export * from './effects';
export * from './selector';
export * from './reducer';

export const effects: any[] = [
    AnggotaEffect
];


import { Action } from '@ngrx/store';
import { Anggota } from '../../models/anggota.model';

export enum AnggotaActionType {
    Load            = '[Anggota] Load',
    LoadSuccess     = '[Anggota] Load Success',
    LoadFailed      = '[Anggota] Load Failed',
    Create          = '[Anggota] Create',
    CreateSuccess   = '[Anggota] Create Success',
    CreateFailed    = '[Anggota] Create Failed',
    Update          = '[Anggota] Update',
    UpdateSuccess   = '[Anggota] Update Success',
    UpdateFailed    = '[Anggota] Update Failed',
    Delete          = '[Anggota] Delete',
    DeleteSuccess   = '[Anggota] Delete Success',
    DeleteFailed    = '[Anggota] Delete Failed'
}

export class Load implements Action {
    readonly type = AnggotaActionType.Load;
    constructor() { }
}

export class LoadSuccess implements Action {
    readonly type = AnggotaActionType.LoadSuccess;
    constructor( public payload: Anggota[]) { }
}
export class LoadFailed implements Action {
    readonly type = AnggotaActionType.LoadFailed;
    constructor( public payload?: any) { }
}

export class Create implements Action {
    readonly type = AnggotaActionType.Create;
    constructor(public payload: Anggota) { }
}

export class CreateSuccess implements Action {
    readonly type = AnggotaActionType.CreateSuccess;
    constructor( public payload: Anggota) { }
}
export class CreateFailed implements Action {
    readonly type = AnggotaActionType.CreateFailed;
    constructor( public payload?: any) { }
}


export class Update implements Action {
    readonly type = AnggotaActionType.Update;
    constructor(public payload: Anggota) { }
}

export class UpdateSuccess implements Action {
    readonly type = AnggotaActionType.UpdateSuccess;
    constructor( public payload: Anggota) { }
}
export class UpdateFailed implements Action {
    readonly type = AnggotaActionType.UpdateFailed;
    constructor( public payload?: any) { }
}

export class Delete implements Action {
    readonly type = AnggotaActionType.Delete;
    constructor(public payload: Anggota) { }
}

export class DeleteSuccess implements Action {
    readonly type = AnggotaActionType.DeleteSuccess;
    constructor( public payload: Anggota) { }
}
export class DeleteFailed implements Action {
    readonly type = AnggotaActionType.DeleteFailed;
    constructor( public payload?: any) { }
}

export type AnggotaAction =
    | Load
    | LoadSuccess
    | LoadFailed
    | Create
    | CreateFailed
    | CreateSuccess
    | Update
    | UpdateFailed
    | UpdateSuccess
    | Delete
    | DeleteFailed
    | DeleteSuccess;

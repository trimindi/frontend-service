import { createSelector } from '@ngrx/store';
import * as fromFeature from './../reducer';
import * as fromAnggota from './../reducer/anggota.reducer';


export const getAnggotaState = createSelector(
    fromFeature.getAnggotaModuleState,
    (state: fromFeature.AnggotaModuleState) => state.anggota
);

export const getAnggotaEntity = createSelector(
    getAnggotaState,
    fromAnggota.getAnggotaEntity
);
export const getAnggotaIsLoaded = createSelector(
    getAnggotaState,
    fromAnggota.getAnggotaIsLoaded
);
export const getAnggotaIsLoading = createSelector(
    getAnggotaState,
    fromAnggota.getAnggotaIsLoading
);

export const getAllAnggota = createSelector(
    getAnggotaEntity,
    entities => {
        return Object.keys(entities).map(id => entities[parseInt(id, 10)]);
    }
);


import { Anggota } from './../../models';
import { AnggotaActionType, AnggotaAction } from './../actions';
import { createSelector } from '@ngrx/store';

export interface AnggotaState {
  entities: { [id: number]: Anggota };
  isLoaded: boolean;
  isLoading: boolean;
  selectedAnggota: Anggota;
}


export const initialState: AnggotaState = {
  entities: {},
  isLoaded: false,
  isLoading: false,
  selectedAnggota: null
};

export function reducer(
  state: AnggotaState = initialState,
  action: AnggotaAction
): AnggotaState {
  switch (action.type) {
    case AnggotaActionType.Load:
      return {
        ...state,
        isLoading: true
      };
    case AnggotaActionType.LoadSuccess:
      const anggotas = action.payload;

      const entities = anggotas.reduce(
        (entities: { [id: number]: Anggota }, anggota: Anggota) => {
          return {
            ...entities,
            [anggota.id]: anggota,
          };
        },
        {
          ...state.entities,
        }
      );

      return {
        ...state,
        isLoading: false,
        isLoaded: true,
        entities
      };
    case AnggotaActionType.LoadFailed:
      return {
        ...state,
        isLoading: false,
        isLoaded: false,
      };
    default:
      return state;
  }
}

export const getAnggotaEntity = (state: AnggotaState) => state.entities;
export const getAnggotaIsLoading = (state: AnggotaState) => state.isLoading;
export const getAnggotaIsLoaded = (state: AnggotaState) => state.isLoaded;
export const getSelectedAnggota = (state: AnggotaState) => state.selectedAnggota;


import { Anggota } from './../../models/anggota.model';
import { createSelector, createFeatureSelector, ActionReducerMap } from '@ngrx/store';
import * as fromAnggota from './anggota.reducer';
import * as fromRoot from '../../../../store/reducers';

export interface AnggotaModuleState {
  anggota: fromAnggota.AnggotaState;
}


export const reducers: ActionReducerMap<AnggotaModuleState> = {
  anggota: fromAnggota.reducer
};

export const getAnggotaModuleState = createFeatureSelector<AnggotaModuleState>('anggota');

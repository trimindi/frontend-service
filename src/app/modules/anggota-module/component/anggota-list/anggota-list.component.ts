import { of } from 'rxjs/observable/of';
import { filter, map, catchError } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Anggota } from './../../models';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import * as fromStore from './../../store';


@Component({
    selector: 'app-anggota-list',
    templateUrl: 'anggota-list.component.html'
})
export class AnggotaListComponent implements OnInit {
    anggotas$: Observable<Anggota[]>;
    constructor(private store: Store<fromStore.AnggotaModuleState>) {}

    ngOnInit() {
        // this.anggotas$     = this.store.select(fromStore.getAllAnggota);
    }
}

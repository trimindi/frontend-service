import { CommonModule } from '@angular/common';
import { FormsModule  } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { effects, reducers } from './store';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { NgModule } from '@angular/core';
import { ComponentModule } from '../../component/component.module';
import { AccountRoutingModule } from './account-routing.module';
import { DataTablesModule } from 'angular-datatables';
import * as fromService from './services';
import * as fromComponent from './component';
import * as fromContainer from './container';
import * as fromUtils from './../../utils';
import * as fromGuards from './guards';
@NgModule({
  imports: [
    DataTablesModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    AccountRoutingModule,
    ComponentModule,
    StoreModule.forFeature('coa', reducers),
    EffectsModule.forFeature(effects),
  ],
  declarations: [
    ...fromComponent.component,
    ...fromContainer.container
  ],
  providers: [...fromService.services, ...fromUtils.inreceptor, ...fromGuards.guards],
  exports: [
    ...fromComponent.component,
    ...fromContainer.container
  ]
})
export class AccountModule { }

import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-dashboard-component',
    templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {
    items = [
        {
            title: 'Account Buku Besar',
            path: '/account/bukubesar',
            image: 'assets/images/avatar/matthew.png',
            description: 'Modul setting account Buku Besar'
        },
        {
            title: 'Account',
            path: '/account/account',
            image: 'assets/images/avatar/matthew.png',
            description: 'Modul setting account'
        }
    ];
    constructor() { }
    ngOnInit() { }
}

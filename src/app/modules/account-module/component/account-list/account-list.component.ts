import { of } from 'rxjs/observable/of';
import { filter, map, catchError } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Account, Jenis, Kelompok, BukuBesar } from './../../models/model';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import * as fromStore from './../../store';
@Component({
    selector: 'app-account-list',
    templateUrl: 'account-list.component.html'
})

export class AccountListComponent implements OnInit {
    jenis$: Observable<Jenis[]>;
    kelompok$: Observable<Kelompok[]>;
    bukubesar$: Observable<BukuBesar[]>;
    account$: Observable<Account[]>;
    selectedBukubesar = '0';
    selectedKelompok = '0';
    selectedJenis = '0';
    constructor(private store: Store<fromStore.CoaState>) {}

    ngOnInit() {
        this.jenis$     = this.store.select(fromStore.getAllJenis);
    }

    onChangeJenis() {
        this.selectedKelompok = '0';
        this.selectedBukubesar = '0';
        this.kelompok$ = new Observable<Kelompok[]>();
        this.kelompok$ = this.store.select(fromStore.getAllKelompok)
        .pipe(
            map(data => {
                return data.filter(d => {
                    return d.id.toString().startsWith(this.selectedJenis.toString());
                });
            }),
            catchError(err => {
                return of(null);
            })
        );
    }
    onChangeKelompok() {
        this.selectedBukubesar = '0';
        this.bukubesar$ = new Observable<BukuBesar[]>();
        this.bukubesar$ = this.store.select(fromStore.getAllBukubesar)
        .pipe(
            map(data => {
                return data.filter(d => {
                    return d.id.toString().startsWith(this.selectedKelompok.toString());
                });
            }),
            catchError(err => {
                return of(null);
            })
        );
    }

    onChangeBukubesar() {
        this.account$ = new Observable<Account[]>();
        this.account$ = this.store.select(fromStore.getAllAccount)
        .pipe(
            map(data => {
                return data.filter(d => {
                    if (d === null) {
                        return false;
                    } else {
                        return d.acc.startsWith(this.selectedBukubesar);
                    }
                });
            }),
            catchError(err => {
                return of(null);
            })
        );
    }
}

import { DashboardComponent } from './dashboard/dashboard.component';
import { AccountListComponent } from './account-list/account-list.component';
import { BukuBesarListComponent } from './buku-besar-list/buku-besar-list.component';

export const component: any[] = [
    AccountListComponent,
    BukuBesarListComponent,
    DashboardComponent
];

export * from './buku-besar-list';
export * from './account-list';
export * from './dashboard';

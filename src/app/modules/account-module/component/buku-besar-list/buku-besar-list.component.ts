import { of } from 'rxjs/observable/of';
import { filter, map, catchError } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Jenis, Kelompok, BukuBesar } from './../../models/model';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import * as fromStore from './../../store';

@Component({
    selector: 'app-bukubesar-list-component',
    templateUrl: 'buku-besar-list.component.html'
})
export class BukuBesarListComponent implements OnInit {
    jenis$: Observable<Jenis[]>;
    kelompok$: Observable<Kelompok[]>;
    bukubesar$: Observable<BukuBesar[]>;

    constructor(private store: Store<fromStore.CoaState>) { }

    ngOnInit() {
        this.jenis$     = this.store.select(fromStore.getAllJenis);
    }

    onChangeJenis($event) {
        let selected = $event.target.value;
        this.kelompok$ = new Observable<Kelompok[]>();
        this.kelompok$ = this.store.select(fromStore.getAllKelompok)
        .pipe(
            map(data => {
                return data.filter(d => {
                    return d.id.toString().startsWith(selected.toString());
                });
            }),
            catchError(err => {
                return of(null);
            })
        );
    }
    onChangeKelompok($event) {
        let selected = $event.target.value;
        this.bukubesar$ = new Observable<BukuBesar[]>();
        this.bukubesar$ = this.store.select(fromStore.getAllBukubesar)
        .pipe(
            map(data => {
                return data.filter(d => {
                    return d.id.toString().startsWith(selected.toString());
                });
            }),
            catchError(err => {
                return of(null);
            })
        );
    }
}

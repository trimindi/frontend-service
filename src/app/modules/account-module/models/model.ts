export interface Account {
    id: number;
    acc: string;
    keterangan: string;
    golongan: string;
    create_at: string;
    update_at: string;
    cif?: string;
    is_deleted?: boolean;
    kode_unit?: string;
}

export interface BukuBesar {
    id: number;
    accbb: string;
    nama: string;
    golongan: string;
    resiko: string;
    create_at: string;
    update_at: string;
    cif: string;
    is_deleted: boolean;
    kode_unit: string;
}

export interface Jenis {
    id: number;
    nama?: string;
    create_at?: string;
    update_at?: string;
    is_deleted?: boolean;
}

export interface Kelompok {
    id: number;
    nama: string;
    gol: string;
    sub_gol: string;
    create_at: string;
    update_at: string;
    is_deleted: boolean;
}

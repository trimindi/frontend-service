import { createSelector } from '@ngrx/store';
import * as fromFeature from './../../store';
import * as fromAccount from './../reducers/account.reducer';
import * as fromKelompok from './../reducers/kelompok.reducer';
import * as fromJenis from './../reducers/jenis.reducer';


export const getAccountState = createSelector(
    fromFeature.getCoaState,
    (state: fromFeature.CoaState) => state.account
);

export const getAccountEntity = createSelector(
    getAccountState,
    fromAccount.getAccountEntity
);
export const getAccountIsLoaded = createSelector(
    getAccountState,
    fromAccount.getAccountIsLoaded
);
export const getAccountIsLoading = createSelector(
    getAccountState,
    fromAccount.getAccountIsLoading
);

export const getAllAccount = createSelector(
    getAccountEntity,
    entities => {
        return Object.keys(entities).map(id => entities[parseInt(id, 10)]);
    }
);


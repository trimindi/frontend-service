import { createSelector } from '@ngrx/store';
import * as fromFeature from './../../store';
import * as fromAccount from './../reducers/account.reducer';
import * as fromKelompok from './../reducers/kelompok.reducer';
import * as fromJenis from './../reducers/jenis.reducer';


export const getJenisState = createSelector(
    fromFeature.getCoaState,
    (state: fromFeature.CoaState) => state.jenis
);

export const getJenisEntity = createSelector(
    getJenisState,
    fromJenis.getJenisEntity
);
export const getJenisIsLoaded = createSelector(
    getJenisState,
    fromJenis.getJenisIsLoaded
);
export const getJenisIsLoading = createSelector(
    getJenisState,
    fromJenis.getJenisIsLoading
);
export const getSelectedJenis  = createSelector(
    getJenisState,
    fromJenis.getSelectedJenis
);

export const getAllJenis = createSelector(
    getJenisEntity,
    entities => {
        return Object.keys(entities).map(id => entities[parseInt(id, 10)]);
    }
);

import { createSelector } from '@ngrx/store';
import * as fromFeature from './../../store';
import * as fromAccount from './../reducers/account.reducer';
import * as fromKelompok from './../reducers/kelompok.reducer';
import * as fromJenis from './../reducers/jenis.reducer';


export const getKelompokState = createSelector(
    fromFeature.getCoaState,
    (state: fromFeature.CoaState) => state.kelompok
);

export const getKelompokEntity = createSelector(
    getKelompokState,
    fromKelompok.getKelompokEntity
);
export const getKelompokIsLoaded = createSelector(
    getKelompokState,
    fromKelompok.getKelompokIsLoaded
);
export const getKelompokIsLoading = createSelector(
    getKelompokState,
    fromKelompok.getKelompokIsLoading
);

export const getAllKelompok = createSelector(
    getKelompokEntity,
    entities => {
        return Object.keys(entities).map(id => entities[parseInt(id, 10)]);
    }
);

export const getAllKelompokBySelectedJenis = createSelector(
    getAllKelompok,
    fromJenis.getSelectedJenis,
    (state , selectedJenis) => {
        if (selectedJenis === undefined) {
            return state;
        }
        return state.filter(data => {
            return data.id.toString().startsWith(selectedJenis.id.toString());
        });
    }
);


import { createSelector } from '@ngrx/store';
import * as fromFeature from './../../store';
import * as fromAccount from './../reducers/account.reducer';
import * as fromKelompok from './../reducers/kelompok.reducer';
import * as fromJenis from './../reducers/jenis.reducer';
import * as fromBukubesar from './../reducers/bukubesar.reducer';

export const getBukubesarState = createSelector(
    fromFeature.getCoaState,
    (state: fromFeature.CoaState) => state.bukubesar
);

export const getBukubesarEntity = createSelector(
    getBukubesarState,
    fromBukubesar.getBukuBesarEntity
);
export const getBukubesarIsLoaded = createSelector(
    getBukubesarState,
    fromBukubesar.getBukuBesarIsLoaded
);
export const getBukubesarIsLoading = createSelector(
    getBukubesarState,
    fromBukubesar.getBukuBesarIsLoading
);

export const getAllBukubesar = createSelector(
    getBukubesarEntity,
    entities => {
        return Object.keys(entities).map(id => entities[parseInt(id, 10)]);
    }
);


import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import { map, switchMap, catchError } from 'rxjs/operators';

import { KelompokActionType } from './../actions';
import * as kelompokAction from './../actions/kelompok.action';
import * as fromServices from '../../services';

@Injectable()
export class KelompokEffect {
  constructor(
    private actions$: Actions,
    private kelompokService: fromServices.KelompokService
  ) {}

  @Effect()
  loadJenis$ = this.actions$.ofType(KelompokActionType.LOAD_KELOMPOK_ACCOUNT).pipe(
    switchMap(() => {
      return this.kelompokService
        .getKelompok()
        .pipe(
          map(kelompok => new kelompokAction.LoadKelompokAccountSuccess(kelompok)),
          catchError(error => of(new kelompokAction.LoadKelompokAccountFail(error)))
        );
    })
  );

}

import { GO } from './../../../../store/actions/router.action';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import { map, switchMap, catchError } from 'rxjs/operators';

import * as fromRoot from './../../../../store';
import { AccountActionType } from './../actions/account.action';
import * as accountAction from './../actions/account.action';
import * as fromServices from '../../services';

@Injectable()
export class AccountEffect {
  constructor(
    private actions$: Actions,
    private accountService: fromServices.AccountService
  ) {}

  @Effect()
  loadAccount$ = this.actions$.ofType(AccountActionType.LOAD_ACCOUNT).pipe(
    switchMap(() => {
      return this.accountService
        .getAccount()
        .pipe(
          map(account => new accountAction.LoadAccountSuccess(account)),
          catchError(error => of(new accountAction.LoadAccountFail(error)))
        );
    })
  );

  @Effect()
  addAccount$ = this.actions$.ofType(AccountActionType.ADD_ACCOUNT).pipe(
    map((action: accountAction.AddAccount) => action.payload),
    switchMap(account => {
      return this.accountService
        .createAccount(account)
        .pipe(
          map(acc => new accountAction.AddAccountSuccess(acc)),
          catchError(error => of(new accountAction.AddAccountFail(error)))
        );
    })
  );

  @Effect()
  createAccountSuccess$ = this.actions$
    .ofType(AccountActionType.ADD_ACCOUNT_SUCCESS)
    .pipe(
      map((action: accountAction.AddAccountSuccess) => action.payload),
      map(account => {
        return new fromRoot.Go({
          path: ['/account/account', account.id],
        });
      })
    );
}

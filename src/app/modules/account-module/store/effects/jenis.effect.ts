import { GO } from './../../../../store/actions/router.action';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import { map, switchMap, catchError } from 'rxjs/operators';

import { JenisActionType } from './../actions';
import * as jenisAction from './../actions/jenis.action';
import * as fromServices from '../../services';

@Injectable()
export class JenisEffect {
  constructor(
    private actions$: Actions,
    private jenisService: fromServices.JenisService
  ) {}

  @Effect()
  loadJenis$ = this.actions$.ofType(JenisActionType.LOAD_JENIS_ACCOUNT).pipe(
    switchMap(() => {
      return this.jenisService
        .getJenis()
        .pipe(
          map(account => new jenisAction.LoadJenisAccountSuccess(account)),
          catchError(error => of(new jenisAction.LoadJenisAccountFail(error)))
        );
    })
  );

}

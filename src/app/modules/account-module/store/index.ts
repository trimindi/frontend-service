
import { AccountEffect, KelompokEffect, JenisEffect, BukubesarEffect } from './effects';
export const effects: any[] = [
    AccountEffect,
    BukubesarEffect,
    KelompokEffect,
    JenisEffect
];

export * from './actions';
export * from './reducers';
export * from './effects';
export * from './selectors';

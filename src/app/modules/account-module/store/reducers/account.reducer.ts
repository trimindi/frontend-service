import { Account } from './../../models';
import { AccountActionType, AccountActions } from '../actions/account.action';
import { createSelector } from '@ngrx/store';

export interface AccountState {
  entities: { [id: number]: Account };
  isLoaded: boolean;
  isLoading: boolean;
  selectedAccountId: number;
}


export const initialState: AccountState = {
  entities: {},
  isLoaded: false,
  isLoading: false,
  selectedAccountId: null
};

export function reducer(
  state: AccountState = initialState,
  action: AccountActions
): AccountState {
  switch (action.type) {
    case AccountActionType.LOAD_ACCOUNT:
      return {
        ...state,
        isLoading: true
      };
    case AccountActionType.LOAD_ACCOUNT_SUCCESS:
      const accounts = action.payload;

      const entities = accounts.reduce(
        (entities: { [id: number]: Account }, account: Account) => {
          return {
            ...entities,
            [account.id]: account,
          };
        },
        {
          ...state.entities,
        }
      );

      return {
        ...state,
        isLoading: false,
        isLoaded: true,
        entities
      };
    case AccountActionType.LOAD_ACCOUNT_FAIL:
      return {
        ...state,
        isLoading: false,
        isLoaded: false,
      };
    default:
      return state;
  }
}

export const getAccountEntity = (state: AccountState) => state.entities;
export const getAccountIsLoading = (state: AccountState) => state.isLoading;
export const getAccountIsLoaded = (state: AccountState) => state.isLoaded;



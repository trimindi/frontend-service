import { createSelector, createFeatureSelector, ActionReducerMap } from '@ngrx/store';
import * as formAccount from './account.reducer';
import * as fromKelompok from './kelompok.reducer';
import * as fromBukubesar from './bukubesar.reducer';
import * as fromJenis from './jenis.reducer';
import * as fromRoot from '../../../../store/reducers';

export interface CoaState {
  account: formAccount.AccountState;
  kelompok: fromKelompok.KelompokState;
  bukubesar: fromBukubesar.BukuBesarState;
  jenis: fromJenis.JenisState;
}


export const reducers: ActionReducerMap<CoaState> = {
  account: formAccount.reducer,
  kelompok: fromKelompok.reducer,
  bukubesar: fromBukubesar.reducer,
  jenis: fromJenis.reducer
};

export const getCoaState = createFeatureSelector<CoaState>('coa');

import { LoadAccountFail } from './../actions/account.action';
import { LoadBukuBesar, BukuBesarActions } from './../actions/bukubesar.action';
import { BukuBesar } from './../../models';
import { BukuBesarActionType } from '../actions';
import { createSelector } from '@ngrx/store';

export interface BukuBesarState {
  entities: { [id: string]: BukuBesar };
  isLoaded: boolean;
  isLoading: boolean;
  selectedAccountId: number;
}


export const initialState: BukuBesarState = {
  entities: {},
  isLoaded: false,
  isLoading: false,
  selectedAccountId: null
};

export function reducer(
  state: BukuBesarState = initialState,
  action: BukuBesarActions
): BukuBesarState {
  switch (action.type) {
    case BukuBesarActionType.LOAD_BUKUBESAR:
      return {
        ...state,
        isLoading: true,
        isLoaded: false
      };
    case BukuBesarActionType.LOAD_BUKUBESAR_SUCCESS:
      const bukubesars = action.payload;

      const entities = bukubesars.reduce(
        (entities: { [id: number]: BukuBesar }, bukubesar: BukuBesar) => {
          return {
            ...entities,
            [bukubesar.id]: bukubesar,
          };
        },
        {
          ...state.entities,
        }
      );

      return {
        ...state,
        isLoading: false,
        isLoaded: true,
        entities
      };
    case BukuBesarActionType.LOAD_BUKUBESAR_FAIL:
      return {
        ...state,
        isLoading: false,
        isLoaded: false,
      };
    default:
      return state;
  }
}

export const getBukuBesarEntity = (state: BukuBesarState) => state.entities;
export const getBukuBesarIsLoading = (state: BukuBesarState) => state.isLoading;
export const getBukuBesarIsLoaded = (state: BukuBesarState) => state.isLoaded;



import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'app-bukubesar-component',
    templateUrl: 'bukubesar.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class BukuBesarComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}

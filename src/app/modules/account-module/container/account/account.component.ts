import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromStore from './../../store';
@Component({
    selector: 'app-account-component',
    templateUrl: 'account.component.html'
})

export class AccountComponent implements OnInit {
    constructor(private store: Store<fromStore.CoaState>) { }

    ngOnInit() {
    }
}

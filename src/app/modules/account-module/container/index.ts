import { BukuBesarComponent } from './bukubersar/bukubesar.component';
import { AccountComponent } from './account/account.component';

export const container: any[] = [
    AccountComponent,
    BukuBesarComponent
];
import { PermisionsGuard } from './guards/permisions.guard';
import { AccountIsLoaded, JenisIsLoaded, KelompokIsLoaded, BukubesarIsLoaded } from './guards';
import { BukuBesarComponent } from './container/bukubersar/bukubesar.component';
import { AccountComponent } from './container/account/account.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';
import { guards } from './guards';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    data: {
      title: 'Dashboard Account'
    }
  },
  {
    path: 'account',
    component: AccountComponent,
    canActivate: [ PermisionsGuard, AccountIsLoaded , JenisIsLoaded, KelompokIsLoaded, BukubesarIsLoaded],
    data: {
      title: 'Account'
    }
  },
  {
    path: 'bukubesar',
    component: BukuBesarComponent,
    canActivate: [ PermisionsGuard, AccountIsLoaded , JenisIsLoaded, KelompokIsLoaded, BukubesarIsLoaded],
    data: {
      title: 'Account Buku Besar'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule {}

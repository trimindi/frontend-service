import { PermisionsGuard } from './permisions.guard';
import { BukubesarIsLoaded } from './bukubesar-loaded.group';
import { JenisIsLoaded } from './jenis-loaded.guard';
import { KelompokIsLoaded } from './kelompok-loaded.guard';
import { AccountIsLoaded } from './account-loaded.guard';
export const guards: any[] = [
    AccountIsLoaded,
    KelompokIsLoaded,
    JenisIsLoaded,
    BukubesarIsLoaded,
    PermisionsGuard
];

export * from './account-loaded.guard';
export * from './bukubesar-loaded.group';
export * from './jenis-loaded.guard';
export * from './kelompok-loaded.guard';
export * from './permisions.guard';


import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot } from '@angular/router';

import { Store } from '@ngrx/store';

import { Observable } from 'rxjs/Observable';
import { tap, map, filter, take, switchMap } from 'rxjs/operators';
import * as fromStore from '../store';
import * as fromRoot from './../../../store';

@Injectable()
export class PermisionsGuard implements CanActivate {
  constructor(private store: Store<fromRoot.State>) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.checkStore();
  }

  checkStore(): Observable<boolean> {
    return this.store.select(fromRoot.getLoginIsLogedIn).pipe(
      tap(loaded => {
        if (!loaded) {
          this.store.dispatch(new fromRoot.Go({
            path: ['/auth', {}],
          }));
        }
      }),
      filter(loaded => loaded),
      take(1)
    );
  }
}

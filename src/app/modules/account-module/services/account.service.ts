import { IResponse } from './../../../models/response.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { catchError, map } from 'rxjs/operators';
import 'rxjs/add/observable/throw';

import { Account } from '../models';

@Injectable()
export class AccountService {
  constructor(private http: HttpClient) {}

  getAccount(): Observable<Account[]> {
    return this.http
      .get<IResponse<Account[]>>(`/api/v1/coa/account`)
      .pipe(
        map(r => r.data)
      );
  }

  createAccount(payload: Account): Observable<Account> {
    return this.http
      .post<IResponse<Account>>(`/api/v1/coa/account`, payload)
      .pipe(
        map(r => r.data),
        catchError((error: any) => Observable.throw(error.json()))
      );
  }

  updateAccount(payload: Account): Observable<Account> {
    return this.http
      .put<IResponse<Account>>(`/api/v1/coa/account/${payload.id}`, payload)
      .pipe(
        map(r => r.data),
        catchError((error: any) => Observable.throw(error.json()))
      );
  }

  removeAccount(payload: Account): Observable<Account> {
    return this.http
      .delete<any>(`/api/v1/coa/account/${payload.id}`)
      .pipe(catchError((error: any) => Observable.throw(error.json())));
  }
}


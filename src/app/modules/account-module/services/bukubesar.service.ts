import { IResponse } from './../../../models/response.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { catchError, map } from 'rxjs/operators';
import 'rxjs/add/observable/throw';

import { BukuBesar } from '../models';

@Injectable()
export class BukubesarService {
  constructor(private http: HttpClient) {}

  getBukabesar(): Observable<BukuBesar[]> {
    return this.http
      .get<IResponse<BukuBesar[]>>(`/api/v1/coa/bukubesar`)
      .pipe(
        map(r => r.data),
        // catchError((error: any) => Observable.throw(error.json()))
      );
  }

  createBukubesar(payload: BukuBesar): Observable<BukuBesar> {
    return this.http
      .post<IResponse<BukuBesar>>(`/api/v1/coa/bukubesar`, payload)
      .pipe(
        map(r => r.data),
        catchError((error: any) => Observable.throw(error.json())));
  }

  updateBukubesar(payload: BukuBesar): Observable<BukuBesar> {
    return this.http
      .put<IResponse<BukuBesar>>(`/api/v1/coa/bukubesar/${payload.id}`, payload)
      .pipe(
        map(r => r.data),
        catchError((error: any) => Observable.throw(error.json())));
  }

  removeBukubesar(payload: BukuBesar): Observable<BukuBesar> {
    return this.http
      .delete<any>(`/api/v1/coa/bukubesar/${payload.id}`)
      .pipe(catchError((error: any) => Observable.throw(error.json())));
  }
}

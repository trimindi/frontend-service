import { NgModule } from '@angular/core';
import { DepositoRoutingModule } from './deposito-routing.module';

@NgModule({
  imports: [
    DepositoRoutingModule
  ],
  declarations: [
  ]
})
export class DepositoModule { }

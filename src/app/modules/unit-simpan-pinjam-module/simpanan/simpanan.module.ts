import { NgModule } from '@angular/core';

import { ApprovalComponent } from './component/approval/approval.component';
import { MasterComponent } from './component/master/master.component';
import { SetoranSimpananAnggotaComponent } from './component/setoran_simpanan_anggota/setoran_simpanan_anggota.component';
import { SetoranSimpananKolektifComponent } from './component/setoran_simpanan_kolektif/setoran_simpanan_kolektif.component';
import { SetoranSimpananPokokComponent } from './component/setoran_simpanan_pokok/setoran_simpanan_pokok.component';
import { SettingComponent } from './component/setting/setting.component';
import { PembatalanTransaksiComponent } from './component/pembatalan_transaksi/pembatalan_transaksi.component';
import { PemblokiranSimpananComponent } from './component/pemblokiran_simpanan/pemblokiran_simpanan.component';
import { PenarikanSimpananComponent } from './component/penarikan_simpanan/penarikan_simpanan.component';
import { PenutupanSimpananComponent } from './component/penutupan_simpanan/penutupan_simpanan.component';
import { PemindahanBukuAntarRekeningComponent } from './component/pemindahan_buku_antar_rekening/pemindahan_buku_antar_rekening.component';
import { PengkreditanBungaComponent } from './component/pengkreditan_bunga_jasa/pengkreditan_bunga_jasa.component';
import { PendebetanBiayaAdminComponent } from './component/pendebetan_biaya_admin/pendebetan_biaya_admin.component';

import { CetakUlangSlipComponent } from './component/cetak_ulang_slip/cetak_ulang_slip.component';


import { SimpananRoutingModule } from './simpanan-routing.module';
import { ComponentModule } from '../../../component/component.module';
@NgModule({
  imports: [
    SimpananRoutingModule,
    ComponentModule
  ],
  declarations: [
    ApprovalComponent,
    MasterComponent,
    SetoranSimpananAnggotaComponent,
    SetoranSimpananKolektifComponent,
    SetoranSimpananPokokComponent,
    PendebetanBiayaAdminComponent,
    SettingComponent,
    PembatalanTransaksiComponent,
    PemblokiranSimpananComponent,
    PenarikanSimpananComponent,
    PenutupanSimpananComponent,
    PemindahanBukuAntarRekeningComponent,
    PengkreditanBungaComponent,
    PendebetanBiayaAdminComponent,
    CetakUlangSlipComponent
  ]
})
export class SimpananModule { }

import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { ApprovalComponent } from './component/approval/approval.component';
import { MasterComponent } from './component/master/master.component';
import { SetoranSimpananAnggotaComponent } from './component/setoran_simpanan_anggota/setoran_simpanan_anggota.component';
import { SetoranSimpananKolektifComponent } from './component/setoran_simpanan_kolektif/setoran_simpanan_kolektif.component';
import { SetoranSimpananPokokComponent } from './component/setoran_simpanan_pokok/setoran_simpanan_pokok.component';
import { SettingComponent } from './component/setting/setting.component';
import { PembatalanTransaksiComponent } from './component/pembatalan_transaksi/pembatalan_transaksi.component';
import { PemblokiranSimpananComponent } from './component/pemblokiran_simpanan/pemblokiran_simpanan.component';
import { PenarikanSimpananComponent } from './component/penarikan_simpanan/penarikan_simpanan.component';
import { PenutupanSimpananComponent } from './component/penutupan_simpanan/penutupan_simpanan.component';
import { PemindahanBukuAntarRekeningComponent } from './component/pemindahan_buku_antar_rekening/pemindahan_buku_antar_rekening.component';
import { PengkreditanBungaComponent } from './component/pengkreditan_bunga_jasa/pengkreditan_bunga_jasa.component';
import { PendebetanBiayaAdminComponent } from './component/pendebetan_biaya_admin/pendebetan_biaya_admin.component';
import { CetakUlangSlipComponent } from './component/cetak_ulang_slip/cetak_ulang_slip.component';



const routes: Routes = [
  {
    path: '',
    redirectTo: 'master',
    pathMatch: 'full',
  },
  {
    path: 'setting',
    component: SettingComponent,
    data: {
      title: 'Setting'
    }
  },
  {
    path: 'master',
    component: MasterComponent,
    data: {
      title: 'Master Simpanan'
    }
  },
  {
    path: 'approval',
    component: ApprovalComponent,
    data: {
      title: 'Approval Master Simpanan'
    }
  },
  {
    path: 'pembatalan',
    component: PembatalanTransaksiComponent,
    data: {
      title: 'Pembatalan Transaksi'
    }
  },
  {
    path: 'cetak_ulang',
    component: CetakUlangSlipComponent,
    data: {
      title: 'Cetak Ulang Slip Component'
    }
  },
  {
    path: 'pemblokiran',
    component: PemblokiranSimpananComponent,
    data: {
      title: 'Pemblokiran Simpanan'
    }
  },
  {
    path: 'penarikan',
    component: PenarikanSimpananComponent,
    data: {
      title: 'Pemblokiran Simpanan'
    }
  },
  {
    path: 'penutupan',
    component: PenutupanSimpananComponent,
    data: {
      title: 'Penutupan Simpanan'
    }
  },
  {
    path: 'pindah_buku',
    component: PemindahanBukuAntarRekeningComponent,
    data: {
      title: 'Pindah Buku'
    }
  },
  {
    path: 'pengkreditan_bunga',
    component: PengkreditanBungaComponent,
    data: {
      title: 'Pengkreditan Bunga'
    }
  },
  {
    path: 'pendebetan_admin',
    component: PendebetanBiayaAdminComponent,
    data: {
      title: 'Pendebetan Biaya Admin'
    }
  },
  {
    path: 'setoran',
    data: {
      title: 'Setoran'
    },
    children: [
      {
        path: 'anggota',
        component: SetoranSimpananAnggotaComponent,
        data: {
          title: 'Setoran Simpanan Anggota'
        }
      },
      {
        path: 'kolektif',
        component: SetoranSimpananKolektifComponent,
        data: {
          title: 'Setorang Simpanan Kolektif'
        }
      },
      {
        path: 'pokok',
        component: SetoranSimpananPokokComponent,
        data: {
          title: 'Setoran Simpanan Pokok'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SimpananRoutingModule {}

import { NgModule } from '@angular/core';
import { HutangRoutingModule } from './hutang-routing.module';

@NgModule({
  imports: [
    HutangRoutingModule,
  ],
  declarations: [
  ]
})
export class HutangModule { }

import { NgModule } from '@angular/core';
import { PiutangRoutingModule } from './piutang-routing.module';

@NgModule({
  imports: [
    PiutangRoutingModule,
  ],
  declarations: [
  ]
})
export class PiutangModule { }

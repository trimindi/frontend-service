export interface Pendidikan {
    id: number;
}

export interface Pekerjaan {
    id: number;
}

export interface Golongan {
    id: number;
}

export interface Identitas {
    id: number;
}

export interface Jabatan {
    id: number;
}

export interface Agama {
    id: number;
}
export interface HubunganKeluarga {
    id: number;
}


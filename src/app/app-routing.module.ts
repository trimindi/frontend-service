import { MasterIsLoaded } from './guards/master.guard';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { LoginFormComponent } from './component/login-form/login-form.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import {
  FullLayoutComponent,
  SimpleLayoutComponent
} from './containers';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FullLayoutComponent,
    canActivate: [ MasterIsLoaded ],
    canActivateChild: [ MasterIsLoaded ],
    data: {
      title: 'Dashboard'
    },
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      }
    ]
  },
  {
    path: 'auth',
    component: SimpleLayoutComponent,
    data: {
      title: 'Auth'
    },
    children: [
      {
        path: '',
        component: LoginFormComponent
      }
    ]
  },
  {
    path: '',
    component: FullLayoutComponent,
    canActivate: [ MasterIsLoaded ],
    canActivateChild: [ MasterIsLoaded ],
    data: {
      title: 'Modul Setting'
    },
    children: [
      {
        path: 'account',
        loadChildren: './modules/account-module/account.module#AccountModule',
        data: {
          title: 'COA'
        },
      },
      {
        path: 'anggota',
        loadChildren: './modules/anggota-module/anggota.module#AnggotaModule',
        data: {
          title: 'Anggota'
        },
      }
    ]
  },
  {
      path: 'usp',
      component : FullLayoutComponent,
      canActivate: [ MasterIsLoaded ],
      canActivateChild: [ MasterIsLoaded ],
      data: {
        title: 'Unit Simpan Pinjam'
      },
      children: [
        {
          path: 'simpanan',
          data: {
            title: 'Simpanan'
          },
          loadChildren: './modules/unit-simpan-pinjam-module/simpanan/simpanan.module#SimpananModule'
        },
        {
          path: 'deposito',
          data: {
            title: 'Deposito'
          },
          loadChildren: './modules/unit-simpan-pinjam-module/deposito/deposito.module#DepositoModule'
        },
        {
          path: 'hutang',
          data: {
            title: 'Hutang'
          },
          loadChildren: './modules/unit-simpan-pinjam-module/hutang/hutang.module#HutangModule'
        },
        {
          path: 'piutang',
          data: {
            title: 'Piutang'
          },
          loadChildren: './modules/unit-simpan-pinjam-module/piutang/piutang.module#PiutangModule'
        }
      ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, { useHash: true }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}

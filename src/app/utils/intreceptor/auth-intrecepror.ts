import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import * as fromRoot from './../../store';
import 'rxjs/add/operator/do';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  token: string;
  constructor(private store: Store<fromRoot.State>) {
  }

  intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.store.select(fromRoot.getLoginAuthToken)
    .subscribe(token => {
      this.token = token;
    });
    if (this.token === undefined) {
      return next.handle(req).do((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          // do stuff with response if you want
        }
      }, (err: any) => {
        console.log('in intreceptor error');
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            this.store.dispatch(new fromRoot.Go({
              path: ['/auth', {}],
            }));
          }
        }
      });
    } else {
      const authReq = req.clone({
        headers: req.headers.set('Authorization', this.token)
      });
      return next.handle(authReq).do((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          // do stuff with response if you want
        }
      }, (err: any) => {
        console.log('in intreceptor error');
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            this.store.dispatch(new fromRoot.Go({
              path: ['/auth', {}],
            }));
          }
        }
      });
    }
  }
}


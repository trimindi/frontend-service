import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './intreceptor/auth-intrecepror';
export const inreceptor: any[] = [
    {
        provide: HTTP_INTERCEPTORS,
        useClass: AuthInterceptor,
        multi: true
    }
];


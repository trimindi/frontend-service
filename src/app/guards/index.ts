import { MasterIsLoaded } from './master.guard';

export const guards: any[] = [
    MasterIsLoaded
];

export * from './master.guard';
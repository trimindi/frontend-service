import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, ActivatedRouteSnapshot } from '@angular/router';

import { Store } from '@ngrx/store';

import { Observable } from 'rxjs/Observable';
import { tap, map, filter, take, switchMap } from 'rxjs/operators';
import * as fromRoot from './../store';

@Injectable()
export class MasterIsLoaded implements CanActivate, CanActivateChild {
  constructor(private store: Store<fromRoot.State>) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.checkStore();
  }
  canActivateChild(
    route: ActivatedRouteSnapshot
  ): Observable<boolean> {
    return this.checkStore();
  }
  checkStore(): Observable<boolean> {
    return this.store.select(fromRoot.getPendidikanIsLoaded).pipe(
      tap(loaded => {
        if (!loaded) {
          this.store.dispatch(new fromRoot.LoadPendidikan());
        }
      }),
      filter(loaded => loaded),
      take(1)
    );
  }
}

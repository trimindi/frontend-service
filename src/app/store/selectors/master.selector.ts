import { createSelector } from '@ngrx/store';
import * as fromRoot from './../reducers';


export const getPendidikanStete = createSelector(
    fromRoot.getMasterState,
    (state) => state.pendidikan
);
export const getPendidikanEntity = createSelector(
    getPendidikanStete,
    (state) => state.entities
);
export const getPendidikanIsLoaded = createSelector(
    getPendidikanStete,
    (state) => state.isLoaded
);
export const getPendidikanIsLoading = createSelector(
    getPendidikanStete,
    (state) => state.isLoading
);
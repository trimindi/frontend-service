import { MasterEffect } from './effects/master.effect';
import { LoginEffect } from './effects/login.effect';
import { RouterEffects } from './effects/router.effect';


export const effects: any[] = [RouterEffects, LoginEffect , MasterEffect];
export * from './actions';
export * from './reducers';
export * from './effects';
export * from './selectors';

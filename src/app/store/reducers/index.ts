import {
    ActionReducerMap,
    createSelector,
    createFeatureSelector,
    ActionReducer,
    MetaReducer,
  } from '@ngrx/store';
  import { environment } from './../../../environments/environment';
  import { RouterStateUrl } from '../../shared/utils';
  import * as fromRouter from '@ngrx/router-store';
  import * as fromLogin from './login.reducer';
  import * as fromMaster from './master.reducer';
  /**
   * storeFreeze prevents state from being mutated. When mutation occurs, an
   * exception will be thrown. This is useful during development mode to
   * ensure that none of the reducers accidentally mutates the state.
   */
  import { storeFreeze } from 'ngrx-store-freeze';

  /**
   * As mentioned, we treat each reducer like a table in a database. This means
   * our top level state interface is just a map of keys to inner state types.
   */
  export interface State {
    routerReducer: fromRouter.RouterReducerState<RouterStateUrl>;
    login: fromLogin.LoginState;
    master: fromMaster.MasterState;
  }

  export const reducers: ActionReducerMap<State> = {
    routerReducer: fromRouter.routerReducer,
    login: fromLogin.reducer,
    master: fromMaster.reducer
  };
  // console.log all

  // console.log all actions
  export function logger(reducer: ActionReducer<State>): ActionReducer<State> {
    return function(state: State, action: any): State {
      console.log('state', state);
      console.log('action', action);
      return reducer(state, action);
    };
  }
  export const getRouterState = createFeatureSelector<
    fromRouter.RouterReducerState<RouterStateUrl>>('routerReducer');
  export const getLoginState = (state: State) => state.login;
  export const getMasterState = (state: State) => state.master;

  /**
   * By default, @ngrx/store uses combineReducers with the reducer map to compose
   * the root meta-reducer. To add more meta-reducers, provide an array of meta-reducers
   * that will be composed to form the root meta-reducer.
   */
  export const metaReducers: MetaReducer<State>[] = !environment.production
    ? [logger, storeFreeze]
    : [];

import {
  Pekerjaan,
  Golongan,
  Agama,
  Pendidikan,
  Identitas,
  Jabatan,
  HubunganKeluarga
} from './../../models/master.model';
import {
  MasterActionType,
  MasterActions
} from '../actions';
import {
  createSelector
} from '@ngrx/store';

export interface PekerjaanState {
  entities: {
    [id: number]: Pekerjaan
  };
  isLoaded: boolean;
  isLoading: boolean;
}
export interface GolonganState {
  entities: {
    [id: number]: Golongan
  };
  isLoaded: boolean;
  isLoading: boolean;
}
export interface AgamaState {
  entities: {
    [id: number]: Agama
  };
  isLoaded: boolean;
  isLoading: boolean;
}
export interface PendidikanState {
  entities: {
    [id: number]: Pendidikan
  };
  isLoaded: boolean;
  isLoading: boolean;
}
export interface IdentitasState {
  entities: {
    [id: number]: Identitas
  };
  isLoaded: boolean;
  isLoading: boolean;
}
export interface JabatanState {
  entities: {
    [id: number]: Jabatan
  };
  isLoaded: boolean;
  isLoading: boolean;
}
export interface HubunganKeluargaState {
  entities: {
    [id: number]: HubunganKeluarga
  };
  isLoaded: boolean;
  isLoading: boolean;
}
export interface MasterState {
  pekerjaan ? : PekerjaanState;
  golongan ? : GolonganState;
  agama ? : AgamaState;
  pendidikan ? : PendidikanState;
  identitas ? : IdentitasState;
  jabatan ? : JabatanState;
  hub_keluarga ? : HubunganKeluargaState;
}


export const initialState: MasterState = {
  pekerjaan: {
    entities: {},
    isLoaded: false,
    isLoading: false
  },
  golongan: {
    entities: {},
    isLoaded: false,
    isLoading: false
  },
  agama: {
    entities: {},
    isLoaded: false,
    isLoading: false
  },
  pendidikan: {
    entities: {},
    isLoaded: false,
    isLoading: false
  },
  identitas: {
    entities: {},
    isLoaded: false,
    isLoading: false
  },
  jabatan: {
    entities: {},
    isLoaded: false,
    isLoading: false
  },
  hub_keluarga: {
    entities: {},
    isLoaded: false,
    isLoading: false
  },
};

export function reducer(
  state: MasterState = initialState,
  action: MasterActions
): MasterState {
  switch (action.type) {
    // case MasterActionType.MASTER_LOAD_PEKERJAAN:
    //   return {
    //     ...state,
    //     pekerjaan: {
    //       ...state.pekerjaan,
    //       isLoaded: false,
    //       isLoading: true
    //     }
    //   };
    // case MasterActionType.MASTER_LOAD_PEKERJAAN_SUCCESS:
    //   let payload = action.payload;

    //   let entities = payload.reduce(
    //     (entities: {
    //       [id: number]: Pendidikan
    //     }, pekerjaan: Pendidikan) => {
    //       return {
    //         ...entities,
    //         [pekerjaan.id]: pekerjaan,
    //       };
    //     }, {
    //       ...state.pendidikan.entities,
    //     }
    //   );

    //   return {
    //     ...state,
    //     pendidikan: {
    //       ...state.pendidikan,
    //       entities,
    //       isLoaded: true,
    //       isLoading: false
    //     }
    //   };
    // case MasterActionType.MASTER_LOAD_PEKERJAAN_FAIL:
    //   return {
    //     ...state,
    //     pekerjaan: {
    //       ...state.pekerjaan,
    //       isLoaded: false,
    //       isLoading: false
    //     }
    //   };
    case MasterActionType.MASTER_LOAD_PENDIDIKAN:
      return {
        ...state,
        pendidikan: {
          ...state.pendidikan,
          isLoaded: false,
          isLoading: true
        }
      };
    case MasterActionType.MASTER_LOAD_PENDIDIKAN_SUCCESS:
      let payload = action.payload;

      let entities = payload.reduce(
        (entities: {
          [id: number]: Pendidikan
        }, pendidikan: Pendidikan) => {
          return {
            ...entities,
            [pendidikan.id]: pendidikan,
          };
        }, {
          ...state.pendidikan.entities,
        }
      );

      return {
        ...state,
        pendidikan: {
          ...state.pendidikan,
          entities,
          isLoaded: true,
          isLoading: false
        }
      };
    case MasterActionType.MASTER_LOAD_PENDIDIKAN_FAIL:
      return {
        ...state,
        pendidikan: {
          ...state.pendidikan,
          isLoaded: false,
          isLoading: false
        }
      };
    default:
      return state;
  }
}

export const getPendidikanEntity = (state: PendidikanState) => state.entities;
export const getPendidikanIsLoading = (state: PendidikanState) => state.isLoaded;
export const getPendidikanIsLoaded = (state: PendidikanState) => state.isLoaded;

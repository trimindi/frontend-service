import { GO } from './../actions';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import { map, switchMap, catchError } from 'rxjs/operators';

import * as fromRoot from './../';
import { LoginActionType } from './../';
import * as loginAction from './../';
import * as fromServices from '../../services';

@Injectable()
export class LoginEffect {
  constructor(
    private actions$: Actions,
    private authService: fromServices.AuthService
  ) {}

  @Effect()
  performLogin$ = this.actions$.ofType(LoginActionType.PERFORM_LOGIN).pipe(
    map((action: loginAction.PerformLogin) => action.payload),
    switchMap(payload => {
      return this.authService
        .performLogin(payload)
        .pipe(
          map(account => new loginAction.PerformLoginSuccess(account)),
          catchError(error => of(new loginAction.PerformLoginFail(error)))
        );
    })
  );
  @Effect()
  performLoginSuccess$ = this.actions$
    .ofType(LoginActionType.PERFORM_LOGIN_SUCCESS)
    .pipe(
      map(account => {
        return new fromRoot.Go({
          path: ['/dashboard', {}],
        });
      })
    );
  @Effect()
  performLogoutSuccess$ = this.actions$
      .ofType(LoginActionType.PERFORM_LOGOUT)
      .pipe(
        map(account => {
          return new fromRoot.Go({
            path: ['/auth', {}],
          });
        })
      );
}

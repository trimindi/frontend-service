import { GO } from './../actions';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import { map, switchMap, catchError } from 'rxjs/operators';

import { MasterActionType } from './../';
import * as masterAction from './../actions/master.action';
import * as fromServices from '../../services';

@Injectable()
export class MasterEffect {
  constructor(
    private actions$: Actions,
    private masterService: fromServices.MasterService
  ) {}

  @Effect()
  loadPendidikan$ = this.actions$.ofType(MasterActionType.MASTER_LOAD_PENDIDIKAN).pipe(
    switchMap(() => {
      return this.masterService
        .getAllPendidikan()
        .pipe(
          map(account => new masterAction.LoadPendidikanSuccess(account)),
          catchError(error => of(new masterAction.LoadPendidikanFail(error)))
        );
    })
  );
}

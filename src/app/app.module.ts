import { services } from './modules/account-module/services/index';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentModule } from './component/component.module';
import { SuiSidebarModule } from 'ng2-semantic-ui';
import { environment } from '../environments/environment'; // Angular CLI environemnt
import { StoreDevtoolsModule  } from '@ngrx/store-devtools';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FullLayoutComponent, SimpleLayoutComponent } from './containers';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { reducers, metaReducers } from './store/reducers';
import { effects } from './store';
import {
  StoreRouterConnectingModule,
  RouterStateSerializer,
} from '@ngrx/router-store';
import { CustomRouterStateSerializer } from './shared/utils';

import * as fromComponent from './component';
import * as fromContainer from './containers';
import * as fromService from './services';
import * as fromUtils from './utils';
import * as fromGuards from './guards';

@NgModule({
  declarations: [
    AppComponent,
    ...fromComponent.components,
    ...fromContainer.containers
  ],
  imports: [
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    ComponentModule,
    SuiSidebarModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    !environment.production ?
      StoreDevtoolsModule.instrument({
        maxAge: 25 // Retains last 25 states
      })
      : [],
    StoreRouterConnectingModule.forRoot({
      stateKey: 'router',
    }),
    EffectsModule.forRoot(effects),
  ],
  providers: [{
    provide: RouterStateSerializer,
    useClass: CustomRouterStateSerializer
  }, ...fromService.services, ...fromUtils.inreceptor, ...fromGuards.guards],
  bootstrap: [AppComponent]
})
export class AppModule { }

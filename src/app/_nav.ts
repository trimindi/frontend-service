export const navigation = [
    {
      name: 'Home',
      url: '/dashboard',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: 'NEW'
      }
    },
    {
      title: true,
      name: 'Modul Anggota'
    },
    {
      name: 'Anggota',
      url: '/usp/simpanan',
      icon: 'home icon',
      children: [
        {
          name: 'Master Anggota',
          url: '/anggota/master',
          icon: 'home icon',
        },
        {
          name: 'Approval',
          url: '/anggota/approval',
          icon: 'home icon'
        },
        {
          name: 'Setting',
          url: '/anggota/setting',
          icon: 'home icon'
        },
        {
          name: 'Rekap',
          url: '/anggota/rekap',
          icon: 'home icon'
        }
      ]
    },
    {
      title: true,
      name: 'Modul Unit Simpan Pinjam'
    },
    {
      name: 'Simpanan',
      url: '/usp/simpanan',
      icon: 'home icon',
      children: [
        {
          name: 'Setting',
          url: '/usp/simpanan/setting',
          icon: 'home icon',
        },
        {
          name: 'Master',
          url: '/usp/simpanan/master',
          icon: 'home icon',
        },
        {
          name: 'Approval',
          url: '/usp/simpanan/approval',
          icon: 'home icon'
        },
        {
          name: 'Setoran Kolektif',
          url: '/usp/simpanan/setoran/kolektif',
          icon: 'home icon'
        },
        {
          name: 'Setoran Pokok',
          url: '/usp/simpanan/setoran/pokok',
          icon: 'home icon',
        },
        {
          name: 'Setoran Anggota',
          url: '/usp/simpanan/setoran/anggota',
          icon: 'home icon'
        },
        {
          name: 'Pendebetan',
          url: '/usp/simpanan/pendebetan',
          icon: 'home icon'
        },
        {
          name: 'Pengkreditan',
          url: '/usp/simpanan/pengkreditan',
          icon: 'home icon',
        },
        {
          name: 'Penarikan',
          url: '/usp/simpanan/penarikan',
          icon: 'home icon'
        },
        {
          name: 'Penutupan',
          url: '/usp/simpanan/penutupan',
          icon: 'home icon',
        },
        {
          name: 'Pemblokiran',
          url: '/usp/simpanan/pemblokiran',
          icon: 'home icon',
        },
        {
          name: 'Pindah Buku',
          url: '/usp/simpanan/pindah_buku',
          icon: 'home icon',
        },
        {
          name: 'Pendebetan Admin',
          url: '/usp/simpanan/pendebetan_admin',
          icon: 'home icon',
        },
        {
          name: 'Pengkreditan Bunga / Jasa',
          url: '/usp/simpanan/penkreditan_bunga',
          icon: 'home icon',
        },
        {
          name: 'Pembatan Transaksi',
          url: '/usp/simpanan/pembatalan',
          icon: 'home icon',
        },
        {
          name: 'Cetak Ulang Slip',
          url: '/usp/simpanan/cetak_ulang',
          icon: 'home icon',
        }
      ]
    },
    {
      name: 'Deposito',
      url: '/usp/deposito',
      icon: 'home icon',
      children: [
        {
          name: 'Setting',
          url: '/usp/deposito/setting',
          icon: 'home icon',
        },
        {
          name: 'Master',
          url: '/usp/deposito/master',
          icon: 'home icon',
        },
        {
          name: 'Approval',
          url: '/usp/deposito/approval',
          icon: 'home icon'
        },
        {
          name: 'Setoran',
          url: '/usp/deposito/setoran',
          icon: 'home icon'
        },
        {
          name: 'Roll Over',
          url: '/usp/deposito/roll_over',
          icon: 'home icon',
        },
        {
          name: 'Pencairan',
          url: '/usp/deposito/pencairan',
          icon: 'home icon',
        },
        {
          name: 'Pembatalan',
          url: '/usp/deposito/pembatalan',
          icon: 'home icon'
        },
        {
          name: 'Cetak Ulang Slip',
          url: '/usp/deposito/cetak_ulang',
          icon: 'home icon'
        }
      ]
    },
    {
      name: 'Piutang',
      url: '/usp/piutang',
      icon: 'home icon',
      children: [
        {
          name: 'Setting',
          url: '/usp/piutang/setting',
          icon: 'home icon',
        },
        {
          name: 'Master',
          url: '/usp/piutang/master',
          icon: 'home icon',
        },
        {
          name: 'Approval',
          url: '/usp/piutang/approval',
          icon: 'home icon'
        },
        {
          name: 'Pencairan',
          url: '/usp/piutang/pencairan',
          icon: 'home icon'
        },
        {
          name: 'Angsuran',
          url: '/usp/piutang/angsuran',
          icon: 'home icon',
        },
        {
          name: 'Angsuran Kolektif',
          url: '/usp/piutang/angsuran_kolektif',
          icon: 'home icon'
        },
        {
          name: 'Perpanjangan',
          url: '/usp/piutang/perpanjangan',
          icon: 'home icon'
        },
        {
          name: 'Restruktur',
          url: '/usp/piutang/restruktur',
          icon: 'home icon',
        },
        {
          name: 'Pelunasan',
          url: '/usp/piutang/pelunasan',
          icon: 'home icon'
        },
        {
          name: 'Pembatalan',
          url: '/usp/piutang/pembatalan',
          icon: 'home icon'
        },
        {
          name: 'Cetak Ulang',
          url: '/usp/piutang/cetak_ulang',
          icon: 'home icon'
        }
      ]
    },
    {
      name: 'Hutang',
      url: '/usp/hutang',
      icon: 'home icon',
      children: [
        {
          name: 'Setting',
          url: '/usp/hutang/setting',
          icon: 'home icon',
        },
        {
          name: 'Kreditur',
          url: '/usp/hutang/kreditur',
          icon: 'home icon',
        },
        {
          name: 'Master',
          url: '/usp/hutang/master',
          icon: 'home icon',
        },
        {
          name: 'Approval',
          url: '/usp/hutang/approval',
          icon: 'home icon'
        },
        {
          name: 'Pencairan',
          url: '/usp/hutang/pencairan',
          icon: 'home icon',
        },
        {
          name: 'Pelunasan',
          url: '/usp/hutang/pelunasan',
          icon: 'home icon',
        },
        {
          name: 'Angsuran',
          url: '/usp/hutang/angsuran',
          icon: 'home icon',
        },
        {
          name: 'Pembatalan',
          url: '/usp/hutang/pembatalan',
          icon: 'home icon'
        },
        {
          name: 'Cetak Ulang',
          url: '/usp/hutang/cetak_ulang',
          icon: 'home icon'
        }
      ]
    }
];
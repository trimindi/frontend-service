import { Store } from '@ngrx/store';
import { Component } from '@angular/core';
import { SuiPopupConfig } from 'ng2-semantic-ui';
import * as fromRoot from './../../store';
@Component({
  selector: 'app-full-layout',
  templateUrl: './full-layout.component.html',
  styleUrls: ['./full-layout.component.css']
})
export class FullLayoutComponent {
  menu = [
    {
      title: 'Module Setting',
      path: '/setting',
      icon: 'home icon'
    },
    {
      title: 'Module Anggota',
      path: '/setting',
      icon: 'home icon'
    },
    {
      title: 'Module Simpanan',
      path: '/setting',
      icon: 'home icon'
    },
    {
      title: 'Module Piutang',
      path: '/setting',
      icon: 'home icon'
    },
    {
      title: 'Module Deposito',
      path: '/setting',
      icon: 'home icon'
    },
    {
      title: 'Module Hutang',
      path: '/setting',
      icon: 'home icon'
    },
    {
      title: 'Module Laporan Keuangan',
      path: '/setting',
      icon: 'home icon'
    }
  ];
  constructor(popupConfig: SuiPopupConfig, private store: Store<fromRoot.State>) {
    popupConfig.isInverted = true;
    popupConfig.delay = 300;
  }
  back() {
    this.store.dispatch(new fromRoot.Back());
  }
  logout() {
    this.store.dispatch(new fromRoot.PerformLogout());
  }
 }
